#!/usr/bin/env bash
INSTANCE=$1
SERVEUR=$2 #gearmand, localhost #f01, f02, f03

NB_WORKER_OK=0
NB_WORKER=0
MSG_WORKER_OK="Workers OK"
MSG_WORKER_CRITICAL=""
MSG_WORKER_UNKNOWN=""

test_gearman_access() {
  statusAcc=2
  statustxtAcc=CRITICAL
  msg1Acc="Gearman access KO. Lancer diagnostic instance pour plus de détail sur ${INSTANCE}"

  # On ping le client Gearman
  # faut mettre gearmand sur mon docker et localhost sur les serveurs
  for JOB in $( (echo status ; sleep 0.1) | nc "${SERVEUR}" 4730 -w 1 | cut -s -f 1)
  do
    statusAcc=0
    statustxtAcc=OK
    msg1Acc="Gearman access ok pour ${INSTANCE}."
    # on récupère la méthode
    IFS="/"
    read -ra ADDR <<< "${JOB}"
    IFS=" "

    # on vérifie que l'instance du job est bien la même que l'instance définit
    JOB_INSTANCE="${ADDR[0]}"
    if [ "${INSTANCE}" != "${JOB_INSTANCE}" ]
    then
      continue
    fi
    METHOD="${ADDR[2]}"
    # on teste chaque worker
    ((NB_WORKER++))
    test_gearman_worker
  done

  if [ ${NB_WORKER} = ${NB_WORKER_OK} ]
  then
    status_worker=0
    NB_WORKER_OK="OK "${NB_WORKER_OK}
  elif [ "${NB_WORKER_OK}" = 0 ]
  then
    status_worker=3
    MSG_WORKER_UNKNOWN="UNKNOWN ERROR SCRIPT worker gearman ne fonctionne pas."
    MSG_WORKER_CRITICAL=""
  else
    status_worker=2
  fi

  echo "$statusAcc Check-Cli-Gearman-Access-${INSTANCE} Gearman-Access=$statustxtAcc $msg1Acc"
  echo "$status_worker Check-Cli-Gearman-Worker-${INSTANCE} Gearman-Worker=${MSG_WORKER_CRITICAL}${MSG_WORKER_UNKNOWN}${NB_WORKER_OK} ${MSG_WORKER_OK} pour ${INSTANCE}."
}

test_gearman_worker() {
    # Récupérer la conf de l'instance + data de test
    DATA="{\"env\":{\"baseDir\":\"\/var\/www\/${INSTANCE}\/\",\"configurationFilename\":\"\/var\/gesica\/conf\/${INSTANCE}.php\",\"configuration\":{\"memcachedServers\":[[\"memcached\",11211,50]]},\"server\":{\"HTTP_HOST\":\"\",\"HTTPS\":null,\"HTTP_X_FORWARDED_PROTO\":null,\"HTTP_USER_AGENT\":\"\",\"HTTP_ACCEPT_LANGUAGE\":\"\",\"HTTP_X_REQUESTED_WITH\":null,\"INSTANCE\":\"${INSTANCE}\",\"SCRIPT_FILENAME\":\"\/var\/www\/${INSTANCE}\/cli.php\",\"SCRIPT_NAME\":\"\/var\/www\/${INSTANCE}\/cli.php\",\"SOURCE\":null,\"SERVER_NAME\":null,\"REQUEST_METHOD\":null,\"REQUEST_URI\":null,\"REMOTE_ADDR\":\"0.0.0.0\",\"REMOTE_PORT\":0,\"SERVER_ADDR\":\"0.0.0.0\",\"SERVER_PORT\":0,\"REQUEST_TIME_FLOAT\":1600451430.28502,\"PHP_AUTH_USER\":null,\"PHP_AUTH_PW\":null,\"HTTP_X_FORWARDED_FOR\":null}},\"data\":{\"isTest\":true,\"method\":\"${JOB}\"}}"
    RESULT=$(gearman -h "${SERVEUR}" -p 4730 -f "${JOB}" "${DATA}" | grep -c "${METHOD}\":true")

    if [ "${RESULT}" -ge 1 ]
    then
      ((NB_WORKER_OK++))
    elif  [ "${RESULT}" -eq 0 ]
    then
      MSG_WORKER_CRITICAL+="CRITICAL ${JOB} KO pour ${INSTANCE}. | "
    fi
}

cd "/var/www/${INSTANCE}" || exit ; INSTANCE=${INSTANCE// /} test_gearman_access
